Warrior

Armor. Short range rockets. Chainsaws.


Magician/Mage

Flamethrowers. Incendiary bombs.


Healer

Lasers. Light bombs aka flash granades.


Ninja/Rogue

Pistols and handguns. Throwing stars, darts. Smoke.


Ranger

Electricity. Long-range and homing missiles. Railguns. Snipers.
